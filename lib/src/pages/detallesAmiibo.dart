import 'package:flutter/material.dart';
import 'package:flutter_application_1/src/models/amiibo_model.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class DetallesAmiibo extends StatelessWidget {
  DetallesAmiibo({Key key, this.amiiboModel});
  final AmiiboModel amiiboModel;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(
      //   title: Text("${this.amiiboModel.name}"),
      // ),
      body: Center(
          child: Container(
        child: GoogleMap(
            initialCameraPosition: CameraPosition(
          target: LatLng(18.5245803, -88.2940831),
          zoom: 15,
        )),
      )),
    );
  }
}

import 'package:flutter/material.dart';

import 'menu.dart';

class OpcionesMenu extends StatefulWidget {
  OpcionesMenu(
      {Key key,
      @required this.nivel,
      @required this.icon,
      @required this.nombre,
      @required this.onTap});
  final int nivel;
  final void Function() onTap;
  final IconData icon;
  final String nombre;
  @override
  _OpcionesMenuState createState() => _OpcionesMenuState();
}

class _OpcionesMenuState extends State<OpcionesMenu> {
  @override
  Widget build(BuildContext context) {
    return Container(
      
      height: 50,
      child: ListTile(
        contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
        leading: Icon(
                widget.icon,
                color: Colors.black54,
              ),
        title: Container(
          child: Text(
            widget.nombre,
            style: TextStyle(color: Colors.black54)
          ),
        ),
        onTap: widget.onTap,
      ),
    );
  }
}

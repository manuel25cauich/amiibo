import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';

AmiiboModel amiiboModelFromJson(String str) =>
    AmiiboModel.fromJson(json.decode(str));
String amiiboModelToJson(AmiiboModel data) => json.encode(data.toJson());

class AmiiboModel {
//datos AmiiboModelles
  String amiiboSeries;
  String character;
  String gameSeries;
  String head;
  String image;
  String name;
  String type;
  String tail;
  Color color;
  bool favorito;
  AmiiboModel(
      {this.amiiboSeries,
      this.character,
      this.gameSeries,
      this.head,
      this.image,
      this.name,
      this.tail,
      this.type,
      this.color,
      this.favorito
      });

  factory AmiiboModel.fromJson(Map<String, dynamic> json) => AmiiboModel(
        amiiboSeries: json["amiiboSeries"],
        character: json["character"],
        gameSeries: json["gameSeries"],
        head: json["head"],
        image: json["image"],
        name: json["name"],
        tail: json["tail"],
        type: json["type"],
        color: Colors.primaries[Random().nextInt(Colors.primaries.length)],
        favorito: false,
      );

  Map<String, dynamic> toJson() => {
        "amiiboSeries": amiiboSeries,
        "character": character,
        "gameSeries": gameSeries,
        "head": head,
        "image": image,
        "name": name,
        "tail": tail,
        "type": type,
      };
}

List<AmiiboModel> amiiboModelsFromJson(String strJson) {
  final str = json.decode(strJson);
  return List<AmiiboModel>.from(str.map((item) {
    return AmiiboModel.fromJson(item);
  }));
}

String amiiboModelsToJson(AmiiboModel cp) {
  final dyn = cp.toJson();
  return json.encode(dyn);
}
